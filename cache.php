<?php

$cache = 'cache';	// Location`
$maxage = '7 days';

if (isset($_GET['load'])) {
	global $cache;
	$city = $_GET['load'];

	gc();	// Delete old cached fiels
	if (file_exists("$cache/$city"))
		echo file_get_contents("$cache/$city");
	else
		header('Cache-control: no-cache');
	exit;
}

if (isset($_POST['save'])) {
	global $cache;
	$city = $_POST['save'];

	if (!file_exists($cache))
		mkdir($cache, 0755, true);
	file_put_contents("$cache/$city", $_POST['data']); 
}

function gc()
{
	global $cache, $maxage;

	if (file_exists($cache))
		foreach (glob("$cache/*") as $file)
			if (filemtime($file) < strtotime("-$maxage"))
				unlink($file);
	
}

?>
