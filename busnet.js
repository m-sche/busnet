function ajax(method, url, input, onready)
{
	var argc = arguments.length;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState === 4 && xmlhttp.status === 200 && argc === 4)
			onready(xmlhttp.response);
	}

	var urn = input.join('&');

	if (method == 'GET') {
		xmlhttp.open(method, urn ? url + '?' + urn : url);
		xmlhttp.send();
	} else {
		xmlhttp.open(method, url);
		xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
		xmlhttp.send(urn);
	}

}

function gebi(id)
{
	return document.getElementById(id);
}

function Link(from, to, len, type)
{
	this.from = from;	// Node
	this.to = to;		// Node
	this.len = len;
	this.type = type;
	this.distance = Infinity;
}

function Node(lat, lon, name)
{
	this.lat = lat;
	this.lon = lon;
	this.links = [];		// Link array
	this.name = name;		// For bus stops
	this.distance = Infinity;	// From nearest bus stop
}

Node.prototype.other = function(link) {
	return (this === link.from ? link.to : link.from);
}

function Map(canvasId)
{
	this.canvas = gebi(canvasId);
	this.maxcoor;		/* [n, e, s, w] */
	this.init();

	this.options = {
		log: true,
		bus_names: false,
		places: false,
		scale: true
	};
}

Map.prototype.init = function() {
	this.clear();
	this.areaname = undefined;
	this.nodes = [];	/* List of Nodes indexed by ID */
	this.links = [];	/* List of Links indexed by number */
	this.stops = [];	/* List of IDs indexed by number */
	this.platforms = [];	/* List of Nodes indexed by number (unmatched bus stops) */
	this.places = [];	/* List of Nodes indexed by number (important locations) */
	this.water = [];	/* List of Links indexed by number */

	this.mplon = 111317;	/* Metres per longitude degree */
	this.mplat = 111317;	/* Metres per latitude degree */

	this.loglines = 0;
}

Map.prototype.clear = function()
{
	this.canvas.innerHTML = '<rect width="100%" height="100%" class="bgrrect"/>';
	this.loglines = 0;
}

Map.prototype.log = function(text)
{
	if (this.option('log')) {
		this.svg_text(10, 20 + this.loglines*12, text, 'logline');
		this.loglines++;
	}
}

Map.prototype.option = function(key, val)
{
	if (val !== undefined)
		this.options[key] = val;
	return this.options[key];
}

Map.prototype.link = function(from, to, type)
{
	// links can lead to nodes, which have not been loaded:
	if (!from || !to)
		return;

	var x = (from.lon - to.lon) * this.mplon;
	var y = (from.lat - to.lat) * this.mplat;
	var dist = Math.sqrt(x*x + y*y);
	var link = new Link(from, to, dist, type);

	this.links.push(link);
	from.links.push(link);
	to.links.push(link);
}

Map.prototype.svg_circle = function(x, y, r, cl)
{
	var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
	circle.setAttribute('cx', x);
	circle.setAttribute('cy', y);
	circle.setAttribute('r', r);
	if (cl) circle.setAttribute('class', cl);
	this.canvas.appendChild(circle);
}

Map.prototype.svg_line = function(x1, y1, x2, y2, cl, color)
{
	var line = document.createElementNS("http://www.w3.org/2000/svg", "line");
	line.setAttribute('x1', x1);
	line.setAttribute('y1', y1);
	line.setAttribute('x2', x2);
	line.setAttribute('y2', y2);
	if (color) line.setAttribute('stroke', color);
	if (cl) line.setAttribute('class', cl);
	this.canvas.appendChild(line);
}

Map.prototype.svg_text = function(x, y, str, cl)
{
	var text = document.createElementNS("http://www.w3.org/2000/svg", "text");
	text.setAttribute('x', x);
	text.setAttribute('y', y);
	text.innerHTML = str;
	if (cl) text.setAttribute('class', cl);
	this.canvas.appendChild(text);
}

Map.prototype.lon2x = function(lon)
{
	return (lon - this.maxcoor.w) * this.canvas.width.baseVal.value / (this.maxcoor.e - this.maxcoor.w);
}

Map.prototype.lat2y = function(lat)
{
	return (this.maxcoor.n - lat) * this.canvas.height.baseVal.value / (this.maxcoor.n - this.maxcoor.s);
}

Map.prototype.draw_node = function(node)
{
	this.svg_circle(this.lon2x(node.lon), this.lat2y(node.lat), 2, 'node');
	this.svg_text(this.lon2x(node.lon), this.lat2y(node.lat), Math.round(node.distance));
}

function rgb(r, g, b)
{
	return "rgb("+r+","+g+","+b+")";
}

function dist2color(dist)
{
	if (dist < 255)
		return rgb(dist, 255, 255-dist);
	else if (dist < 511)
		return rgb(255, 511-dist, dist-255);
	else if (dist < 767)
		return rgb(767-dist, 0, 767-dist);
	else
		return 'black';
}

Map.prototype.draw_scale = function()
{
	var width = this.canvas.getAttribute('width');
	for (var i = 0; i <= 768; i += 10) {
		var x = width*(786+i)/(3*768);
		var tick = (i % 50 == 0);
		this.svg_line(x, 10, x, 20 + 5*tick, 'way', dist2color(i));
		if (i % 100 == 0)
			this.svg_text(x, 34, i, 'scale');
	}
}

Map.prototype.draw_water = function(link)
{
	this.svg_line(
		this.lon2x(link.from.lon),
		this.lat2y(link.from.lat),
		this.lon2x(link.to.lon),
		this.lat2y(link.to.lat),
		link.type);
	return;
}

Map.prototype.draw_link = function(link)
{
	var dist = Math.round((link.from.distance + link.to.distance) / 2);
	var color = dist2color(dist);
	var cl = 'way';

	switch (link.type) {
	case 'motorway':
	case 'trunk':
	case 'primary':
	case 'secondary':
	case 'tertiary':
		cl = 'way_major';
		break;
	case 'service':
	case 'track':
	case 'cycleway':
	case 'footway':
	case 'steps':
	case 'path':
		cl = 'way_minor';
		break;
	default:
		cl = 'way';
	}

	this.svg_line(
		this.lon2x(link.from.lon),
		this.lat2y(link.from.lat),
		this.lon2x(link.to.lon),
		this.lat2y(link.to.lat),
		cl, color);
}

Map.prototype.draw_stop = function(drawn, node, cl)
{
	this.svg_circle(this.lon2x(node.lon), this.lat2y(node.lat), 3, cl);
	if (this.option('bus_names') && node.name && !drawn[node.name])
		this.svg_text(this.lon2x(node.lon)+5, this.lat2y(node.lat)+4, node.name, cl);
}

Map.prototype.draw_place = function(node, cl)
{
	var name = node.name.replace('/\s/\n/g', node.name);
	this.svg_text(this.lon2x(node.lon), this.lat2y(node.lat), name, cl);
}

Map.prototype.draw = function()
{
	this.canvas.setAttribute('width', this.canvas.getBBox().width);		// SVG size = HTML size
	var map_ratio = this.mplat * (this.maxcoor.n - this.maxcoor.s) / this.mplon / (this.maxcoor.e - this.maxcoor.w);
	this.canvas.setAttribute('height', Math.round(this.canvas.getBBox().width * map_ratio));

	var perf_start = performance.now();

	/* Draw rivers */
	for (var i in this.water)
		if (this.water[i])
			this.draw_water(this.water[i]);

	/* Draw links */
	for (var i in this.links)
		if (this.links[i])
			this.draw_link(this.links[i]);

	var perf_links = performance.now();
	this.log('[' + Math.round(perf_links-perf_start) + 'ms] links drawn');

	/* Draw nodes */
	//for (var id in this.nodes)
	//	if (this.nodes[id])
	//		this.draw_node(this.nodes[id]);

	/* Draw places */
	if (this.option('places'))
		for (var pl of this.places)
			this.draw_place(pl, 'place');

	/* Draw bus/tram stops */
	var drawn_stops = {};
	for (var i in this.stops) {
		var node = this.nodes[this.stops[i]];
		this.draw_stop(drawn_stops, node, 'bus');
		drawn_stops[node.name] = true;
	}
	drawn_stops.length = 0;
	for (var pf of this.platforms)
		this.draw_stop(pf, 'platform');

	this.log('[' + Math.round(performance.now()-perf_links) + 'ms] nodes drawn');

	if (this.option('scale'))
		this.draw_scale();
}

function distance(node1, node2)
{
	var d_lon = (node1.lon-node2.lon) / this.mplon;
	var d_lat = (node1.lat-node2.lat) / this.mplat;
	return Math.sqrt(d_lon*d_lon + d_lat*d_lat);
}

Map.prototype.place_stops = function(platforms)
{
	var perf_start = performance.now();

	var data = [];

	for (var id in this.nodes) {
		var node = this.nodes[id];
		/* Exclude nodes, which are identical to the stops */
		if (node && node.links.length)
			data.push([node.lon * this.mplon, node.lat * this.mplat, id]);
	}

	var qtree = d3.quadtree(data);

	var perf_tree = performance.now();
	this.log('[' + Math.round(perf_tree-perf_start) + 'ms] quadtree built');

	for (var pf of platforms) {
		var nn = qtree.find(pf.lon * this.mplon, pf.lat * this.mplat)[2];
		this.nodes[nn].name = pf.name;
		this.stops.push(nn);
	}

	this.log('[' + Math.round(performance.now()-perf_tree) + 'ms] ' + platforms.length + ' platforms estimated');
}

Map.prototype.preoptimize = function(minlen)
{
	var count = 0;
	var perf_start = performance.now();

	for (var id in this.nodes) {
		var node = this.nodes[id];
		if (node.links.length === 2
		&& node.links[0].type === node.links[1].type
		&& node.links[0].len + node.links[1].len < minlen) {
			this.link(node.other(node.links[0]), node.other(node.links[1]), node.links[0].type);
			node.links[0].len = node.links[1].len = -1;
			node.links[0].from = node.links[0].to = undefined;
			node.links[1].from = node.links[1].to = undefined;
			this.nodes[id] = undefined;	// The node is freed from memory now
			count++;
		}
	}

	this.links = this.links.filter(function(link){return (link.len !== -1);});	// The links are freed from memory now

	this.log('[' + Math.round(performance.now()-perf_start) + 'ms] shortcut ' + count + ' pairs of links');
}

Map.prototype.postoptimize = function(maxdistance) {
	var countn = 0;
	var countl = 0;

	var perf_start = performance.now();

	/* Mark too distant nodes */
	for (var id in this.nodes) {
		if (this.nodes[id] === undefined)
			continue;
		if (this.nodes[id].distance > maxdistance) {
			countn++;
			this.nodes[id].distance = -1;
			this.nodes[id].links.length = 0;
			this.nodes[id] = undefined;
		}
	}

	/* Delete assigned links */
	for (var l in this.links) {
		if (this.links[l] && (
		this.links[l].from.distance === -1 ||
		this.links[l].to.distance === -1)) {
			this.links[l] = undefined;	// The link and possibly the node are freed from memory now
			countl++;
		}
	}

	/* Recompute map boundaries */
	this.maxcoor = {n: -90, e: -270, s: 90, w: 270};
	for (var id in this.nodes) {
		var elem = this.nodes[id];
		if (elem === undefined) continue;
		if (elem.lat > this.maxcoor.n) this.maxcoor.n = elem.lat;
		if (elem.lon > this.maxcoor.e) this.maxcoor.e = elem.lon;
		if (elem.lat < this.maxcoor.s) this.maxcoor.s = elem.lat;
		if (elem.lon < this.maxcoor.w) this.maxcoor.w = elem.lon;
	}

	this.log('[' + Math.round(performance.now()-perf_start) + 'ms] cropped ' + countn + ' nodes and ' + countl + ' links');
	if (countn + countl)
		console.log('bbox (nesw) = (' + this.maxcoor.n + ', ' + this.maxcoor.e +
			', ' + this.maxcoor.s + ', ' + this.maxcoor.w + ')');

}

Map.prototype.load_data = function(data, area_name)
{
		var platforms = [];	// List of temporary Nodes

		var perf_start = performance.now();

		this.maxcoor = {n: -90, e: -270, s: 90, w: 270};
		for (var i = 0; i < data.elements.length; i++) {
			var elem = data.elements[i];

			if (elem.type === 'node') {
				if (elem.tags && elem.tags.place) {
					this.places.push(new Node(elem.lat, elem.lon, elem.tags.name));
				} else {
					/* Bus stops may be part of a way, we have to add them to both */
					if (elem.tags && (elem.tags.highway === 'bus_stop' || elem.tags.railway == 'tram_stop'))
						platforms.push(new Node(elem.lat, elem.lon, elem.tags.name));
					this.nodes[elem.id] = new Node(elem.lat, elem.lon);
				}

				/* Maximal coordinates */
				if (elem.lat > this.maxcoor.n) this.maxcoor.n = elem.lat;
				if (elem.lon > this.maxcoor.e) this.maxcoor.e = elem.lon;
				if (elem.lat < this.maxcoor.s) this.maxcoor.s = elem.lat;
				if (elem.lon < this.maxcoor.w) this.maxcoor.w = elem.lon;

			} else if (elem.type === 'way') {
				for (var n = 1; n < elem.nodes.length; n++) {
					if (elem.tags.highway)
						this.link(this.nodes[elem.nodes[n-1]], this.nodes[elem.nodes[n]], elem.tags.highway);
					else
						this.water.push(new Link(this.nodes[elem.nodes[n-1]], this.nodes[elem.nodes[n]], 0, elem.tags.waterway));
				}
			}
		}

		this.areaname = area_name;

		this.log('[' + Math.round(performance.now()-perf_start) + 'ms] ' +
			Object.keys(this.nodes).length + ' nodes and ' +
			this.links.length + ' links loaded');

		console.log('bbox (nesw) = (' + this.maxcoor.n + ', ' +
			this.maxcoor.e + ', ' + this.maxcoor.s + ', ' + this.maxcoor.w + ')');

		this.preoptimize(30);	// Minimal length of a link (shorter will be joined together)

		this.place_stops(platforms);
		platforms.length = 0;

		this.dijkstra();
		this.postoptimize(2000);	// Maximal distance of the node from the nearest stop
		this.set_mpdeg();
		this.draw();
}

Map.prototype.build_query = function(city)
{
	switch (city.osm_type) {
	case 'relation':
		// Get everything inside the area specified by a relation
		return "[out:json][timeout:10];" +
		"relation(" + city.osm_id + ");" +
		"(._;>;);" +
		"map_to_area->.a;" +
		"(" +
		  "node(area.a)[highway=bus_stop];" +
		  "node(area.a)[railway=tram_stop];" +
		");" +
		"(" +
		  "._;" +
		  "way(around:800)[highway];" +
		  "node(area.a)[place];" +
		  "way(area.a)[waterway~'stream|river$'];" +
		");" +
		"(._;>;);" +
		"out qt;";
	case 'node':
	case 'way':
		// Get everything in a distance of 800m from a point
		return "[out:json][timeout:10];" +
		city.osm_type + "(" + city.osm_id + ");" +
		"(" +
		  "node(around:800)[highway=bus_stop];" +
		  "node(around:800)[railway=tram_stop];" +
		");" +
		"(" +
		  "._;" +
		  "way(around:800)[highway];" +
		  "node(around:800)[place];" +
		  "way(around:800)[waterway~'stream|river$'];" +
		");" +
		"(._;>;);" +
		"out qt;";
	default:
		return null;
	}
}

Map.prototype.set_mpdeg = function()
{
	this.mplon = this.mplat * Math.cos((this.maxcoor.n + this.maxcoor.s) / 2 * Math.PI / 180);
	console.log('1 lon deg = '+this.mplon+' km');
}

Map.prototype.load_city = function(request, ondone)
{
	var perf_start = performance.now();

	this.init();
	this.log('Request for ' + request);

	/* Search for the city name */
	ajax('GET', 'http://nominatim.openstreetmap.org/search', ['q='+request, 'format=json', 'limit=1'], function(out) {
		var results = JSON.parse(out);
		if (results.length === 0 || this.build_query(results[0]) === null) {
			if (ondone) ondone('No suitable area found');
			return;
		}
		var city = results[0];

		var perf_search = performance.now();
		this.log('[' + Math.round(perf_search-perf_start) + 'ms] ' + city.osm_type + ' with id ' + city.osm_id + ' found');

		/* Try to obtain given relation from the cache */
		ajax('GET', 'cache.php', ['load='+city.osm_id], function(cached) {
			if (cached) {
				this.log('[' + Math.round(performance.now()-perf_search) + 'ms] fetched from cache');
				this.load_data(JSON.parse(cached), request);
				if (ondone) ondone();
			} else {
				var perf_cache = performance.now();
				this.log('[' + Math.round(perf_cache-perf_search) + 'ms] cache miss');

				/* Download new data */
				ajax('POST', 'http://overpass-api.de/api/interpreter', ['data='+this.build_query(city)], function(out) {
					var data = JSON.parse(out);
					if (data.elements.length) {
						this.log('[' + Math.round(performance.now()-perf_cache) + 'ms] fetched from overpass');
						ajax('POST', 'cache.php', ['save='+city.osm_id, 'data='+out]);	// Cache downloaded data
						this.load_data(data, request);
						if (ondone) ondone();
					} else {
						this.log('[' + Math.round(performance.now()-perf_cache) + 'ms] overpass timed out');
						if (ondone) ondone('Requested area is too large');
					}
					ajax('GET', 'http://overpass-api.de/api/kill_my_queries', []);	// Prevent multiple queries
				}.bind(this));
			}
		}.bind(this));
	}.bind(this));
}

Map.prototype.dijkstra = function()
{
	var perf_start = performance.now();

	var queue = new FastPriorityQueue(function(a, b){ return a.distance < b.distance; });

	for (var b of this.stops) {
		this.nodes[b].distance = 0;
		queue.add(this.nodes[b]);
	}

	while (!queue.isEmpty()) {
		var node = queue.poll();
		for (var link of node.links) {
			if (link === undefined)
				continue;
			var node2 = node.other(link);
			if (node2 === undefined)
				continue;
			if (node.distance + link.len < node2.distance) {
				node2.distance = node.distance + link.len;
				queue.add(node2);
			}
		}
	}

	this.log('[' + Math.round(performance.now()-perf_start) + 'ms] dijkstra\'s algorithm');
}

var map = new Map('canvas');

function swap_option(opt)
{
	map.option(opt, !map.option(opt));
	if (map.areaname) {
		map.clear();
		map.draw();
	}
}

var svg_relevant_styles = {'rect':['fill','stroke','stroke-width'],'path':['fill','stroke','stroke-width'],'circle':['fill','stroke','stroke-width'],'line':['stroke','stroke-width'],'text':['fill',"font-size",'font-weight','text-anchor'],'polygon':['stroke','fill']};

function style_element(node, original)
{
	for (var cd = 0; cd < node.childNodes.length; cd++) {
		var child = node.childNodes[cd];
		var tag = child.tagName;

		if (tag in ['svg', 'g']) {
			style_element(child, original.childNodes[cd])
		} else if (tag in svg_relevant_styles) {
			var style = window.getComputedStyle(original.childNodes[cd]);
			var styleString = "";
			for (var property of svg_relevant_styles[tag])
				styleString += property + ":" + style.getPropertyValue(property) + "; ";
			child.setAttribute("style", styleString);
			child.removeAttribute('class');
		}
	}
}

function save_svg(svg)
{
	var odom = svg.cloneNode(true);
	style_element(odom, svg);
	var data = new XMLSerializer().serializeToString(odom);
	var blob = new Blob([data], { type: "image/svg+xml;charset=utf-8" });
	var url = URL.createObjectURL(blob);
	var link = document.createElement('a');
	link.setAttribute('href', url);
	link.setAttribute('download', "busnet.svg");
	link.style.display = 'none';
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
}

window.onload = function() {
	gebi('search').onclick = function(ev) {
		var request = gebi('input_city').value;
		document.body.style.cursor = 'wait';
		map.load_city(request, function(err){
			document.body.style.cursor = 'default';
			if (err) map.log(err);
		});
	};

	gebi('cb_log').onclick = function(){swap_option('log')};
	gebi('cb_bus_names').onclick = function(){swap_option('bus_names')};
	gebi('cb_places').onclick = function(){swap_option('places')};
	gebi('cb_scale').onclick = function(){swap_option('scale')};
	gebi('cb_save').onclick = function(){save_svg(gebi('canvas'))};
}

/*
 * Statistics (Trebic):
 * 1-linked nodes:  8 %
 * 2-linked nodes: 67 %
 * 3-linked nodes: 21 %
 * polynked nodes:  4 %
 */
