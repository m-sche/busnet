<!DOCTYPE html>
<html lang="cs-CZ">

<head>
<meta charset="UTF-8">
<meta name="author" content="Martin Scheubrein">
<title>Busnet</title>
<link rel="stylesheet" href="./style.css" type="text/css">
</head>

<body>

<div id="sidebar">
<h2><img src="bus_stop_symbol.svg"/>BUSNET</h2>
<input type="checkbox" autocomplete="off" id="cb_log" checked><label for="cb_log">Zobraz log</label>
<input type="checkbox" autocomplete="off" id="cb_bus_names"><label for="cb_bus_names">Názvy zastávek</label>
<input type="checkbox" autocomplete="off" id="cb_places"><label for="cb_places">Názvy míst</label>
<input type="checkbox" autocomplete="off" id="cb_scale" checked><label for="cb_scale">Měřítko</label>
<label id="cb_save">Uložit obrázek</label>
</div>

<div id="main">

<form id="form" action="#">
<input id="input_city" type="text" placeholder="Praha,Letňany | Třebíč..."/>
<button id="search">Vyhledat</button>
</form>

<svg id="canvas" xmlns="http://www.w3.org/2000/svg"></svg>

</div>

<script src="priorityqueue.js"></script>
<script src="quadtree.js"></script>
<script src="busnet.js"></script>

</body>

</html>
